<?php

namespace App\Http\Controllers;

use App\Models\EntidadesComerciale;
use App\Models\LicenciasReba;
use Illuminate\Http\Request;

class LicenciasRebaController extends Controller
{

    public function index()
    {
        $licenciasReba = LicenciasReba::paginate();

        return view('licencias-reba.index', compact('licenciasReba'))
            ->with('i', (request()->input('page', 1) - 1) * $licenciasReba->perPage());
    }

    public function create()
    {
        $licenciasReba = new LicenciasReba();
        return view('licencias-reba.create', compact('licenciasReba'));
    }


    public function store(Request $request)
    {
        $licenciasReba = LicenciasReba::create($request->all());

        $entidadComercial = EntidadesComerciale::find($request->id);
        $entidadComercial->licenciaReba_id = $licenciasReba->id;
        $entidadComercial->save();

        return redirect()->route('licencias-reba.index', compact('licenciasReba'))
            ->with('success', 'Licencia Reba creada correctamente.');
    }

    public function show($id)
    {
        $licenciasReba = LicenciasReba::find($id);

        return view('licencias-reba.show', compact('licenciasReba'));
    }

    public function edit($id)
    {
        $licenciasReba = LicenciasReba::find($id);

        return view('licencias-reba.edit', compact('licenciasReba'));
    }


    public function update(Request $request, LicenciasReba $licenciasReba)
    {
//        request()->validate(LicenciasReba::$rules);


        $licenciasReba->update($request->all());

        return redirect()->route('licencias-reba.index', compact('licenciasReba'))
            ->with('success', 'Licencia Reba actualizada correctamente');
    }

    public function destroy($id)
    {
        $licenciasReba = LicenciasReba::find($id);
        $licenciasReba['borrado'] = 1;
        $licenciasReba->save();

        return redirect()->route('licencias-reba.index')
            ->with('success', 'Licencia Reba eliminada correctamente');
    }
}
