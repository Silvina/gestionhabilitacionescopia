<?php

namespace App\Http\Controllers;

use App\Models\Vehiculo;
use App\Models\Persona;
use Faker\Provider\Person;
use Illuminate\Http\Request;


class VehiculoController extends Controller
{

    public function index()
    {
        $vehiculos = Vehiculo::paginate();

        return view('vehiculo.index', compact('vehiculos'))
            ->with('i', (request()->input('page', 1) - 1) * $vehiculos->perPage());
    }

    public function create()
    {
        $vehiculo = new Vehiculo();
        $persona = Persona::all();

        return view('vehiculo.create', compact('vehiculo'),compact('persona'));
    }

    public function store(Request $request)
    {
        request()->validate(Vehiculo::$rules);

        $vehiculo = Vehiculo::create($request->all());


        return redirect()->route('vehiculo.index')
            ->with('success', 'Vehiculo creado correctamente.');
    }

    public function show($id)
    {
        $vehiculo = Vehiculo::find($id);

        return view('vehiculo.show', compact('vehiculo'));
    }

    public function edit($id)
    {
        $vehiculo = Vehiculo::find($id);
        $persona = Persona::all();

        return view('vehiculo.edit', compact('vehiculo'),compact('persona'));
    }

    public function update(Request $request, Vehiculo $vehiculo)
    {
        //request()->validate(Vehiculo::$rules);

        $vehiculo->update($request->all());

        return redirect()->route('vehiculos.index')
            ->with('success', 'Vehiculo actualizado correctamente');
    }

    public function destroy($id)
    {
        $vehiculo = Vehiculo::find($id);
        $vehiculo['borrado'] = 1;
        $vehiculo->save();

        return redirect()->route('vehiculos.index')
            ->with('success', 'Vehiculo borrado correctamente');
    }
}
