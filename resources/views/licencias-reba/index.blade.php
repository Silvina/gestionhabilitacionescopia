@extends('adminlte::page')

@section('title', 'Licencias Reba')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Licencias Reba') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('licenciasReba.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
										<th>Categoria</th>
										<th>Fecha Otorg.</th>
										<th>Fecha Venc.</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($licenciasReba as $licenciaReba)

                                        <tr>
											<td>{{ $licenciaReba->categoria }}</td>
											<td>{{ date('d-m-Y', strtotime($licenciaReba->fechaOtorgamiento )) }}</td>
											<td>{{ date('d-m-Y', strtotime($licenciaReba->fechaVencimiento  )) }}</td>

                                            <td>
                                                <form action="{{ route('licenciasReba.destroy',$licenciaReba->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('licenciasReba.show',$licenciaReba->id) }}"><i class="fa fa-fw fa-eye"></i></a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('licenciasReba.edit',$licenciaReba->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $licenciasReba->links() !!}
            </div>
        </div>
    </div>
@endsection
