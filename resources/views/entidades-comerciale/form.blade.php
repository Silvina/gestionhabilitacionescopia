<div class="box box-info padding-1">
    <div class="box-body">

        <div class="row" >
            <div class="form-group col-3">
                {{ Form::label('tipo') }}
                <select id="tipo" name="tipo" class="form-control" value="{{$entidadesComerciale->tipo}}" onchange="mostrar(this.value)">
                    <option value="#">Seleccione una opcion</option>
                    <option value="comercio">COMERCIO</option>
                    <option value="remiseria">REMISERIA</option>
                    <option value="taxi">TAXI</option>
                </select>
            </div>
        </div>

      <!--  <div class="row">
            <div class="form-group  col-3">
            <input type="button"  class="btn btn-warning pull-right" data-toggle="modal" data-target="#modalTitular" placeholder="Nuevo Titular" value="Nuevo Titular"></input>
        </div>
        </div>-->

        <div class="row" >

            <div class="form-group col-3" id="nombre" style="display: none;">
                {{ Form::label('Nombre') }}
                {{ Form::text('nombre', $entidadesComerciale->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre']) }}
            </div>

            <div class="form-group col-3" id="legajo" style="display: none;">
                {{ Form::label('Legajo') }}
                {{ Form::text('legajo', $entidadesComerciale->legajo, ['class' => 'form-control' . ($errors->has('legajo') ? ' is-invalid' : ''), 'placeholder' => 'Legajo']) }}
            </div>

            <div class="form-group col-3" id="expediente" style="display: none;">
                {{ Form::label('Expediente') }}
                {{ Form::text('expediente', $entidadesComerciale->expediente, ['class' => 'form-control' . ($errors->has('expediente') ? ' is-invalid' : ''), 'placeholder' => 'Expediente']) }}
            </div>

            <div class="form-group col-3" id="rubro" style="display: none;">
                {{ Form::label('Rubro') }}
                {{ Form::text('rubro', $entidadesComerciale->rubro, ['class' => 'form-control' . ($errors->has('rubro') ? ' is-invalid' : ''), 'placeholder' => 'Rubro']) }}
            </div>

            <div class="form-group col-3" id="domicilio" style="display: none;">
                {{ Form::label('Domicilio') }}
                {{ Form::text('domicilio', $entidadesComerciale->domicilio, ['class' => 'form-control' . ($errors->has('domicilio') ? ' is-invalid' : ''), 'placeholder' => 'Domicilio']) }}
            </div>

            <div class="form-group col-3" id="parada" style="display: none;">
                {{ Form::label('parada') }}
                {{ Form::text('parada', $entidadesComerciale->parada, ['class' => 'form-control' . ($errors->has('parada') ? ' is-invalid' : ''), 'placeholder' => 'Parada']) }}
            </div>


            <div class="form-group col-2" id="partida" style="display: none;">
                {{ Form::label('Partida') }}
                {{ Form::text('partida', $entidadesComerciale->partida, ['class' => 'form-control' . ($errors->has('partida') ? ' is-invalid' : ''), 'placeholder' => 'Partida']) }}
            </div>




            <div class="form-group col-3" id="chofer" style="display: none;">
                {{ Form::label('Chofer') }}
                <select class="form-control" name="chofer_id" id="chofer_id">
                    @foreach($personas as $chofer)
                        <option value="{{$chofer['id']}}"> {{$chofer['apellido']}}, {{$chofer['nombre']}} </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-2" id="categoriasComerciales" style="display: none;">
                {{ Form::label('Cat. Comercial') }}
                <select class="form-control" name="categoriasComerciales" id="categoriasComerciales">
                        @foreach($categorias as $cc)
                            <option value="{{$cc['id']}}"> {{$cc['nombre']}} </option>
                        @endforeach
                </select>
            </div>


        </div>


    </div>
</div>

    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Guardar</button>
        @if($entidadesComerciale->id && $entidadesComerciale->tipo == 'comercio')
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalTitular">Cargar Titular/es</button>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalReba">Licencia REBA</button>
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalComercial">Habilitacion Com.</button>
         @endif
        @if($entidadesComerciale->id && ($entidadesComerciale->tipo == 'taxi' || $entidadesComerciale->tipo == 'remiseria'))-->
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalVehiculo">Cargar Vehiculo</button>
        <@endif

    </div>
    </div>

</div>


<!-- Modals -->
<div id="modalReba" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cargar Licencia REBA</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                    <form method="POST" action="{{ route('licenciasReba.store') }}"  role="form" enctype="multipart/form-data">
                        @csrf

                        @include('licencias-reba.form')

                    </form>

            </div>

        </div>

    </div>
</div>

<div id="modalTitular" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nuevo Titular</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="modalComercial" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cargar Habilitacion Comercial</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('habilitacionesComerciales.store') }}"  role="form" enctype="multipart/form-data">
                    @csrf

                    @include('habilitaciones-comerciale.form')

                </form>
            </div>

        </div>

    </div>
</div>

<div id="modalVehiculo" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Vehículos</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>

        </div>

    </div>

</div>
<!-- FIN MODALS -->

<script type="text/javascript" src="js/jquery.js"></script>

<script type="text/javascript">
    function mostrar(id) {

        if (id == "taxi") {
            $("#nombre").hide();
            $("#legajo").show();
            $("#expediente").show();
            $("#rubro").show();
            $("#domicilio").hide();
            $("#parada").show();
            $("#fechaInicio").show();
            $("#partida").hide();
            $("#fechaVencimiento").hide();
            $("#tipoHab").hide();
            $("#vencimientoVtv").show();
            $("#vencimientoLicencia").show();
            $("#vencimientoSeguro").show();
            $("#chofer").show();
            $("#dominio").show();
        //    $("#vehiculoContratado").show();
            $("#marca").show();
            $("#modelo").show();
           // $("#modalTitular").hide();
            $("#categoriasComerciales").hide();

        }

        if (id == "comercio") {
            $("#nombre").show();
            $("#legajo").show();
            $("#expediente").show();
            $("#rubro").show();
            $("#domicilio").show();
            $("#parada").hide();
            $("#fechaInicio").show();
            $("#partida").show();
            $("#fechaVencimiento").show();
            $("#tipoHab").show();
            $("#vencimientoVtv").hide();
            $("#vencimientoLicencia").hide();
            $("#vencimientoSeguro").hide();
            $("#chofer").hide();
            $("#dominio").hide();
           // $("#vehiculoContratado").hide();
           // $("#modalTitular").show();
            $("#categoriasComerciales").show();

        }

        if (id == "remiseria") {
            $("#nombre").show();
            $("#legajo").show();
            $("#expediente").show();
            $("#rubro").show();
            $("#domicilio").show();
            $("#parada").show();
            $("#fechaInicio").show();
            $("#partida").hide();
            $("#fechaVencimiento").hide();
            $("#tipoHab").hide();
            $("#vencimientoVtv").hide();
            $("#vencimientoLicencia").hide();
            $("#vencimientoSeguro").hide();
            $("#chofer").hide();
            $("#dominio").hide();
         //   $("#vehiculoContratado").hide();
         //   $("#modalTitular").hide();
            $("#categoriasComerciales").hide();

        }

    }
</script>





